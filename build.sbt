scalaVersion := "2.12.8"

name := "triggerise-local-store"
organization := "com.org.triggerise"
version := "1.0"

//configuration lib
libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.11.1"
//logger lib
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.8.0"

// Add the ScalaMock library (versions 4.0.0 onwards)
libraryDependencies += "org.scalamock" %% "scalamock" % "4.1.0" % Test
// also add ScalaTest as a framework to run the tests
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % Test