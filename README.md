# Triggerise Local Store

Exercise create for the interview. Exercise specification on the following [link](https://github.com/Triggerise/challenges/blob/master/triggerise-be-technical-interview.md)
The application does not make currency conversions and in the command line interactive ui is showing the values in euros with PT locale format

## Configuration

```json
triggerise-local-store {
    //define the list of products of local store
    products : [
        {
            code: "TICKET"
            name: "Triggerise Ticket"
            price: "5.00"
        },
        {
            code: "HOODIE"
            name: "Triggerise Hoodie"
            price: "20.00"
        },
        {
            code: "HAT"
            name: "Triggierse Hat"
            price: "7.50"
        }
    ]

    //define the checkout discount rules
    rules {
        two-for-one {
            products: [
                "TICKET"
            ]
        }
        bundle-discount {
            bundle-products: [
                {
                    code: "HOODIE"
                    discount-amount: "1"
                    min-qty: 3
                }
            ]
        }
    }    
}
```

## Project Tree

    .
    ├── ...
    ├── src                     
    │   └── main
    │      ├── resources         #location of configuration file              
    │      └── scala             # src code 
    │         ├── di             # dependency injector configuration
    │         │  └── ...        
    |         ├── store          #package with logical code of the local store
    |         |  ├── discount    #package that contains the cart discount rules strategies
    |         |  |  └── ...
    |         |  ├── models      #package that contains the representation of local store objects
    |         |  |  └── ...
    |         |  └── services    #package that contains the services to customer make a purchase in the local store
    |         |     └── ...    
    |         └── Main.scala    
    ├── test                     #package with unit tests             
    ├── build.sbt                #file to declare the external dependencies of the project
    └── DocketFile               #docker image to deploy the application

## Dependencies

- Java
- SBT
- Docker (for deployment)

## Installation

Use the package manager sbt to execute the application

```bash
$sbt run
```

## Run tests

```bash
$sbt clean coverage test
$sbt coverageReport
```

## Create the docker image artifact and executes the program

```bash
#command to create a fat jar of the application
$sbt assembly
#builds the image
$docker build -t triggerise/local-store:v1 .
#executes the application
$docker run -it triggerise/local-store:v1
```