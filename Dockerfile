FROM openjdk:8-jre-slim
ADD target/scala-**/triggerise-local-store-assembly-1.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]