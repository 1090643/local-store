import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

class DiscountRuleServiceTest extends FlatSpec with MockFactory {

  "DiscountRuleService" should "return some discount rules" in {
    val discountRuleService =
      new DiscountRuleService(Set(stub[DiscountStrategy]))
    assert(discountRuleService.discountRules.size == 1)
  }

}
