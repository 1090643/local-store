import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

class CheckoutTest extends FlatSpec with MockFactory {

  def stubProductService = {
    val productServiceStub = stub[ProductService]
    (productServiceStub.product _)
      .when("TICKET")
      .returns(
        Some(new Product("TICKET", "Triggerise Ticket", 5.00))
      )
    productServiceStub
  }

  def stubDiscountStrategy(
      amount: BigDecimal,
      basket: Basket
  ): DiscountStrategy = {
    var discountStrategy = stub[DiscountStrategy]
    (discountStrategy.runRule _)
      .when(basket)
      .returns(
        List[DiscountDetails](
          new DiscountDetails(amount)
        )
      )
    discountStrategy
  }

  def stubDiscountRuleService(basket: Basket, amount: BigDecimal) = {
    val discountRuleService = stub[DiscountRuleService]
    (discountRuleService.discountRules _)
      .when()
      .returns(
        Set[DiscountStrategy](
          stubDiscountStrategy(amount, basket)
        )
      )
    discountRuleService
  }

  "Checkout" should "return 5€ of total when buying 2 Tickets (5€ of discount)" in {
    val discountAmount: BigDecimal = 5
    val basket = new Basket()
    val checkout = new Checkout(
      stubDiscountRuleService(basket, discountAmount),
      stubProductService,
      basket
    )
    checkout.scan("TICKET")
    checkout.scan("TICKET")
    assert(checkout.total == 5)
  }

  "Checkout" should "return 10€ of total when buying 2 Tickets (0€ of discount)" in {
    val discountAmount: BigDecimal = 0
    val basket = new Basket()
    val checkout = new Checkout(
      stubDiscountRuleService(basket, discountAmount),
      stubProductService,
      basket
    )
    checkout.scan("TICKET")
    checkout.scan("TICKET")
    assert(checkout.total == 10)
  }

  "Checkout" should "throw Exception when the cart has an invalid product" in {
    val discountAmount: BigDecimal = 0
    val basket = new Basket()
    val checkout = new Checkout(
      stubDiscountRuleService(basket, discountAmount),
      stubProductService,
      basket
    )
    checkout.scan("TICKET")
    checkout.scan("FAKE-TICKET")
    
    assertThrows[IllegalArgumentException] {
        checkout.total
    }
  }  
}
