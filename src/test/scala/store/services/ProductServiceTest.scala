import org.scalatest.FlatSpec

class ProductServiceTest extends FlatSpec {

    "ProductService" should "return Some product info" in {
        val productService = new ProductService(Set(new Product("TICKET", "Ticket", 5.00)))
        assert(productService.product("TICKET").head.code == "TICKET")
    }

    "ProductService" should "return None product info" in {
        val productService = new ProductService(Set(new Product("TICKET", "Ticket", 5.00)))
        assert(productService.product("TICKET2") == None)
    }

    "ProductService" should "return list with catalog members" in {
        val productService = new ProductService(Set(new Product("TICKET", "Ticket", 5.00)))
        assert(productService.products.size == 1)
    }
}