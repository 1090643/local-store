import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

class BundleDiscountTest extends FlatSpec with MockFactory {

  def bundleProducts = Set(BundleProducts("HOODIE", 1, 3))

  def stubBasket(productCode: String, qty: Int): Basket = {
    val basket = stub[Basket]
    (basket.productQtyInBasket _) when (productCode) returns (qty)
    basket
  }

  "BundleDiscount" should "return discount amount of 4 euros when you buy 4 HOODIES" in {
    val bundleDiscount = new BundleDiscount(bundleProducts)
    val discountList = bundleDiscount.runRule(stubBasket("HOODIE", 4))
    assert(discountList.head.discount == 4)
  }

  "BundleDiscount" should "return empty discount list when you buy 4 TICKETS" in {
    val bundleDiscount = new BundleDiscount(bundleProducts)
    val discountList = bundleDiscount.runRule(stubBasket("HOODIE", 0))
    assert(discountList.size == 0)
  }

  "BundleDiscount" should "return empty discount list when the min qty of the bundle is not reached" in {
    val bundleDiscount = new BundleDiscount(bundleProducts)
    val discountList = bundleDiscount.runRule(stubBasket("HOODIE", 2))
    assert(discountList.size == 0)
  }
}
