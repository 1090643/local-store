import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

class TwoForOneDiscountTest extends FlatSpec with MockFactory {

  val productTwoForOne = Set("TICKET")
  val ticketPrice = 5

  def stubProductService = {
    val productServiceStub = stub[ProductService]
    (productServiceStub.product _)
      .when("TICKET")
      .returns(
        Some(new Product("TICKET", "Triggerise Ticket", 5.00))
      )
    productServiceStub
  }

  def stubProductServiceWithEmptyCatalog = {
    val productServiceStub = stub[ProductService]
    (productServiceStub.product _)
      .when("TICKET")
      .returns(
        None
      )
    productServiceStub
  }

  def stubBasket(productCode: String, qty: Int): Basket = {
    val basket = stub[Basket]
    (basket.productQtyInBasket _) when (productCode) returns (qty)
    basket
  }

  "TwoForOneDiscount" should "return no discount amount when you buy 1 TICKET" in {
    val twoForOneDiscount =
      new TwoForOneDiscount(productTwoForOne, stubProductService)
    val discountList = twoForOneDiscount.runRule(stubBasket("TICKET",1))
    assert(discountList.size == 0)
  }

  "TwoForOneDiscount" should "return exception when product code exist in the basket and discount configuration and not in product catalog" in {
    val twoForOneDiscount =
      new TwoForOneDiscount(
        productTwoForOne,
        stubProductServiceWithEmptyCatalog
      )
    assertThrows[IllegalStateException] {
      twoForOneDiscount.runRule(stubBasket("TICKET",3))
    }
  }

  "TwoForOneDiscount" should "return 1 free product discount amount when you buy 2 TICKETS" in {
    val twoForOneDiscount =
      new TwoForOneDiscount(productTwoForOne, stubProductService)
    val discountList = twoForOneDiscount.runRule(stubBasket("TICKET",2))
    assert(discountList.head.discount == ticketPrice * 1)
  }

  "TwoForOneDiscount" should "return 1 free product discount amount when you buy 3 TICKETS" in {
    val twoForOneDiscount =
      new TwoForOneDiscount(productTwoForOne, stubProductService)
    val discountList = twoForOneDiscount.runRule(stubBasket("TICKET",3))
    assert(discountList.head.discount == ticketPrice * 1)
  }

  "TwoForOneDiscount" should "return 2 free products discount amount when you buy 4 TICKETS" in {
    val twoForOneDiscount =
      new TwoForOneDiscount(productTwoForOne, stubProductService)
    val discountList = twoForOneDiscount.runRule(stubBasket("TICKET",4))
    assert(discountList.head.discount == ticketPrice * 2)
  }

  "TwoForOneDiscount" should "return 2 free products discount amount when you buy 5 TICKETS" in {
    val twoForOneDiscount =
      new TwoForOneDiscount(productTwoForOne, stubProductService)
    val discountList = twoForOneDiscount.runRule(stubBasket("TICKET",5))
    assert(discountList.head.discount == ticketPrice * 2)
  }
}
