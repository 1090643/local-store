import org.scalatest.FlatSpec

class BasketTest extends FlatSpec {

  "Basket" should "return the qty of products with the same code" in {
    val basket = new Basket()
    basket.addElemToBasket("TICKET")
    basket.addElemToBasket("TICKET")
    assert(basket.productQtyInBasket("TICKET") == 2)
  }

}
