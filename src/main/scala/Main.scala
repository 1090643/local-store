import scala.util.control.Breaks._
import com.typesafe.scalalogging.Logger

object Main extends App {
  val logger = Logger("triggerise-local-store")

  val EXIT_COMMAND = "exit"
  val PROCCED_PAYMENT_COMMAND = "pay"
  val products = DependencyInjector.productService.products
  val currencyService = DependencyInjector.currencyService

  def welcomeScreen() {
    logger.info("----------------------------------------------------------")
    logger.info("Triggerise local Store -----------------------------------")
    logger.info("----------------------------------------------------------")
    logger.info("1 - New checkout")
    logger.info("2 - Exit")
    logger.info("----------------------------------------------------------")
    scala.io.StdIn.readInt() match {
      case 1 =>
        newPurchase()
      case 2 =>
        logger.info("Have a nice day")
        System.exit(0)
      case _ =>
        logger.info("Invalid Option")
    }
  }

  def printProducts() {
    logger.info("Catalog --------------------------------------------------")
    for (product <- products) {
      logger.info(s"${product.code} - ${product.name}(${currencyService.printAmountWithRightCurrencyFormat(product.price)})")
    }
    logger.info("----------------------------------------------------------")
  }

  def printBasket(co: Checkout) {
    logger.info("Basket  --------------------------------------------------")
    for (basketItem <- co.basket.products) {
      println(s"${basketItem}")
    }
    logger.info("----------------------------------------------------------")
  }

  def printProductsAndBasket(co: Checkout) {
    printProducts()
    printBasket(co)
    logger.info(
      s"To exit checkout please write '${EXIT_COMMAND}' or to processed with payment please write '${PROCCED_PAYMENT_COMMAND}'"
    )
  }

  def newPurchase():Boolean = {
    val co = DependencyInjector.checkoutService
    while (true) {
      printProductsAndBasket(co)
      scala.io.StdIn.readLine match {
        case PROCCED_PAYMENT_COMMAND =>
          if (co.basket.products.nonEmpty) {
            logger.info("Processing checkout")
            printBasket(co)
            val total = co.total
            logger.info(s"Your checkout total is ${currencyService.printAmountWithRightCurrencyFormat(total)}. Have a nice day!")
          } else {
            logger.info(
              "You don't pay nothing, because you don't have items in checkout basket. Have a nice day!"
            )
          }
          return true
        case EXIT_COMMAND =>
        logger.info("Have a nice day!")
          System.exit(0)
        case product =>
          if (products.filter(p => p.code == product).nonEmpty) {
            co.scan(product)
          } else {
            logger.info("Invalid product code, please try again.")
          }
      }
    }
    false
  }

  while (true) {
    welcomeScreen
  }
}
