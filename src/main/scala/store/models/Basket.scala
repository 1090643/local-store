import scala.collection.mutable

/** The basket representation in triggerise local store
  *
  *  @constructor create a new basket
  */
class Basket {
  var _productsList: mutable.MutableList[String] = mutable.MutableList[String]()

  def addElemToBasket(code: String) = _productsList.+=(code)

  def productQtyInBasket(productCode: String) =
    products.filter(p => p == productCode).size

  def products: List[String] = List.empty ++ _productsList
}
