/** An discount details about the information that was process by some checkout rule 
 *
 *  @constructor create a new discount info to be applied on the checkout
 *  @param discount the discount amount applied by some rule
 */
class DiscountDetails(val discount: BigDecimal)