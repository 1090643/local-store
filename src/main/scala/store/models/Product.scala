/** The product representation in triggerise local store
  *
  *  @constructor create a new product
  *  @param code the product's code name
  *  @param age the product's code name
  *  @param price the product's price
  */
class Product(val code: String, val name: String, val price: BigDecimal)