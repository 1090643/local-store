import scala.collection.mutable
import com.typesafe.scalalogging.Logger

/** An two for one discount strategy to check if there some products with free price
  * inside of checkout
  *
  *  @constructor create a new checkout rule
  *  @param products list of products codes where is possible to have 1 product free
  *     by buying two
  *  @param productService service that contains the catalog of the local store
  */
class TwoForOneDiscount(
    products: Set[String],
    productService: ProductService
) extends DiscountStrategy {

  val logger = Logger[BundleDiscount]

  /** Check the product qty in the checkout and returns the qty of free products
    *
    * @param productCode product code that the two-for-one discount is available
    * @param basket with list of products in the checkout
    * @return qty of free products
    */
  def qtyElementsForFree(
      productCode: String,
      basket: Basket
  ): Int = {
    val qty = basket.productQtyInBasket(productCode)
    if (qty > 0 && qty.%(2) == 0) {
      return qty./(2)
    } else if (qty - 1 > 1 && (qty - 1).%(2) == 0) {
      return (qty - 1)./(2)
    }
    return 0
  }

  /** Runs the two for one discount strategy
    *
    * Runs the discount rule to check there is some products that must be set as free,
    * if there is it will return the discount amount to apply on checkout total
    *
    * @param basket with list of products in the checkout
    * @throws IllegalStateException basket product and twoforone product does not exist in product catalog
    * @return a list of discount details to apply on the checkout total
    */
  def runRule(basket: Basket): List[DiscountDetails] = {
    val discountDetails = mutable.MutableList[DiscountDetails]()
    for (productCode <- products) {
      val qty = qtyElementsForFree(productCode, basket)
      if (qty > 0) {
        productService.product(productCode) match {
          case Some(p) =>
            val discountDetail = new DiscountDetails(p.price.*(qty))
            logger.debug(
              s"The two for one discount strategy was applied $qty free products on $productCode item." +
                s" The discount amount is ${discountDetail.discount} euros"
            )
            discountDetails += discountDetail
          case _ =>
            throw new IllegalStateException(
              "The application is not properly well configured," +
                s"because the product $productCode exist in the basket and discount rule and not in the product catalog"
            )
        }
      }
    }
    List.empty ++ discountDetails
  }
}
