import scala.collection.mutable
import com.typesafe.scalalogging.Logger

/** Class configuration for bundle discount strategy
  *
  *  @constructor create a new bundle discount strategy configuration
  *  @param code product's name
  *  @param discountAmount amount value to apply a discount by each item
  *  @param minQty the minimum quantity of elements of the same product the checkout
  *      must have to be considered as "bundle"
  */
case class BundleProducts(code: String, discountAmount: BigDecimal, minQty: Int)

/** An bundle discount strategy to check if there is an bundle of products inside of the checkout.
  *
  *  @constructor create a new checkout rule
  *  @param bundleProducts list of bundle products to apply the discount in the checkout
  */
class BundleDiscount(bundleProducts: Set[BundleProducts]) extends DiscountStrategy {

  val logger = Logger[BundleDiscount]

  /** Runs the bundle discount strategy
    *
    * Runs the discount rule to check there is some bundle products, if there is
    * it will return the discount amount to apply on checkout total
    *
    * @param basket with list of products in the checkout
    * @return a list of discount details to apply on the checkout total
    */
  def runRule(basket: Basket): List[DiscountDetails] = {
    var discountDetails: mutable.MutableList[DiscountDetails] =
      mutable.MutableList[DiscountDetails]()
    for (bundleProduct <- bundleProducts) {
      val qty = basket.productQtyInBasket(bundleProduct.code)
      if (qty >= bundleProduct.minQty) {
        val discountDetail = new DiscountDetails(
          bundleProduct.discountAmount.*(qty)
        )
        logger.debug(s"Bundle discount strategy will return ${discountDetail.discount} euros of discount on $bundleProduct product")
        discountDetails += discountDetail
      }
    }
    List.empty ++ discountDetails
  }
}
