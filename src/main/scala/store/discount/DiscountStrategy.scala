/** Representation of some discount checkout rule
  *
  */
abstract class DiscountStrategy {

  /** Runs the discount strategy
    *
    * Runs the discount rule to check the checkout and returs the discount
    * amount to apply on checkout total
    *
    * @param basket with list of products in the checkout
    * @return a list of discount details to apply on the checkout total
    */
  def runRule(basket: Basket): List[DiscountDetails]
}
