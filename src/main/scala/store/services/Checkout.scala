import scala.collection.mutable.MutableList
import com.typesafe.scalalogging.Logger

/** Checkout that calculates the total to pay by the client
  *
  *  @constructor create a new checkout to calculate the total amount to pay
  *  @param discountRules service that contains the discounts rules definition
  *   to apply in the checkout
  *  @param productService service that contains the catalog of the local store
  *  @param basket empty basket to add products that the client is going to buy
  */
class Checkout(
    discountRuleService: DiscountRuleService,
    productService: ProductService,
    val basket: Basket
) {
  val logger = Logger[Checkout]

  logger.info("New checkout created")

  /** Adds a new product in the checkout
    *
    * @param productRef product code to add in the checkout
    */
  def scan(productRef: String) = {
    logger.info(s"New product added to the basket: $productRef")
    basket.addElemToBasket(productRef)
  }

  /** Calculate the checkout total
    *
    * @return checkout total with discounts applied
    */
  def total: BigDecimal = {
    logger.info(s"Calculating checkout totals")
    var _discountDetails = MutableList[DiscountDetails]()
    for (pricingRule <- discountRuleService.discountRules)
      _discountDetails ++= pricingRule.runRule(basket)
    val total = _totalWithoutDiscount.-(_discountAmount(_discountDetails))
    logger.debug(s"The checkout total with discount is $total euros")
    total
  }

  /** Calculate the checkout total without discount
    *
    * @return checkout total without discounts applied
    */
  def _totalWithoutDiscount = {
    var total: BigDecimal = 0
    for (prod <- basket.products) {
      productService.product(prod) match {
        case Some(p) =>
          total = total.+(p.price)
        case _ =>
          throw new IllegalArgumentException("The product code insert in the basket does not exists")
      }
    }
    logger.info(s"The checkout grand total (without discount) is $total euros")
    total
  }

  /** Sums all the discount amounts calculate by discount rule strategy
    *
    * @return total discount amount to apply in the checkout
    */
  def _discountAmount(discountDetails: MutableList[DiscountDetails]) = {
    var discountTotal: BigDecimal = 0
    for (discount <- discountDetails) {
      discountTotal = discountTotal.+(discount.discount)
    }
    logger.debug(s"The sum of all discount is $discountTotal euros")
    discountTotal
  }
}
