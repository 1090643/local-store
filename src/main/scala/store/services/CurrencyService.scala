import java.util.{Currency, Locale}

/** Service the currency configuration to display the values
  *
  *  @constructor create a new currency service
  */
class CurrencyService {
    private val _pt = Currency.getInstance(new Locale("pt", "PT"))
    private val _formatter = java.text.NumberFormat.getCurrencyInstance

    _formatter.setCurrency(_pt)

    /** Filters the product catalog by product code
    *
    * @param amount to apply the right format
    * @return amount with the right format for the current currency and locale
    */
    def printAmountWithRightCurrencyFormat(amount: BigDecimal) = _formatter.format(amount)
}