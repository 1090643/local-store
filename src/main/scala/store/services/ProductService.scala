/** Product service that contains the representation of products in the local store
  *
  *  @constructor create a new product service
  *  @param productsList catalog of products inside of the local store
  */
class ProductService(productsList: Set[Product]) {

  /** Gets the products catalog of the local store
    *
    * @return a list of discount details to apply on the checkout total
    */
  def products: Set[Product] = productsList

  /** Filters the product catalog by product code
    *
    * @param code product code to filter
    * @return Option with the product information or not
    */
  def product(code: String): Option[Product] =
    products.filter(p => p.code == code).headOption
}
