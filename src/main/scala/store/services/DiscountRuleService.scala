/** Service that contains the list of discount rules to apply in the checkout
  *
  *  @constructor create a new discount service
  *  @param discountRules discount rules to apply in the checkout
  */
  class DiscountRuleService(discountRulesList: Set[DiscountStrategy]) {
    def discountRules = discountRulesList
}