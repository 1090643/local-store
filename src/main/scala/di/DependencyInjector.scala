import pureconfig._
import pureconfig.generic.auto._

case class Config(triggeriseLocalStore: TriggeriseLocalStore)
case class TriggeriseLocalStore(products: Set[Product], rules: RulesConfig)
case class RulesConfig(twoForOne: TwoForOneDiscountConfig, bundleDiscount: BundleDiscountConfig)
case class BundleDiscountConfig(bundleProducts: Set[BundleProducts])
case class TwoForOneDiscountConfig(products: Set[String])

/** Dependency Injector design pattern to get the list of products of local store
  * and the checkout discount rules
  */
object DependencyInjector {

  private var config = loadConfigOrThrow[Config]

  /** Builds the two-for-one discount rule
    *
    *  @return two-for-one discount rule strategy
    */
  def twoForOneDiscountStrategy:DiscountStrategy = new TwoForOneDiscount(config.triggeriseLocalStore.rules.twoForOne.products, productService)

  /** Builds the bundle discount rule
    *
    *  @return bundle discount rule strategy
    */
  def bundleDiscount:DiscountStrategy = new BundleDiscount(config.triggeriseLocalStore.rules.bundleDiscount.bundleProducts)

  /** Builds the discounts rules
    *
    *  @return list of discount rules strategies
    */
  def discountRules: DiscountRuleService = new DiscountRuleService(Set(twoForOneDiscountStrategy, bundleDiscount))

  /** Builds the product service that contains the representation of products inside local store
    *
    *  @return product service
    */
  def productService = new ProductService(config.triggeriseLocalStore.products)

  /** Builds the checkout service to a client be able to buy products in the local store
    *
    *  @return checkout service
    */  
  def checkoutService = new Checkout(discountRules, productService, basket)

  /** Builds the checkout service to a client be able to buy products in the local store
    *
    *  @return basket service
    */  
  def basket = new Basket()

  /** Builds the currency service to display the right format values to the client
    *
    *  @return currency service
    */   
  def currencyService = new CurrencyService()
}
